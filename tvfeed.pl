#!/bin/perl

use strict;
use warnings;

use LWP::Simple;
use JSON;

use Date::Manip;

use URI::Escape;
use File::HomeDir;
use File::Slurp;
use File::Basename;
use File::Path qw/make_path/;
use Getopt::Std;
use utf8;
use Encode qw(encode_utf8);

binmode STDOUT, ':encoding(UTF-8)';

 
my $global;
our ($opt_a, $opt_d, $opt_l, $opt_v, $opt_n, $opt_r, $opt_u, $opt_f, $opt_p, $opt_q);

sub runCommand
{
  my $command = shift;

  if ($opt_v)
  {
    print join(' ', @$command) . "\n\n";
  }
  if ($opt_n)
  {
    return;
  }
  my $pid = fork();
  if (defined $pid && $pid == 0)
  {
      # child
      exec(@{$command});
      exit 0;
  }
}

sub downloadItem
{
  my $dir = shift;
  my $item = shift;
  my $stream = shift;

  my $target = $dir . $item->{'title'} . '.' . $stream->{'meta'}->{'suffixHint'};


#   if ($opt_v)
#   {
    print "Downloading stream: '$target' ($stream->{'meta'}->{'quality'})\n";
#   }
  my $command;
  
  if ($stream->{'url'} =~ m/^rtmp/)
  {  
    my @streamdata_raw = split(/ /, $stream->{'url'});
    my $streamdata = {};
    foreach my $sd (@streamdata_raw)
    {
      if ($sd =~ m/^rtmp.*/)             {
        $streamdata->{'url'} = $sd;
      } elsif ($sd =~ m/^playpath=(.*)/) {
        $streamdata->{'playpath'} = $1;
      } elsif ($sd =~ m/^swfUrl=(.*)/)   {
        $streamdata->{'swfUrl'} = $1;
      } elsif ($sd =~ m/^swfVfy=(.*)/)   {
        $streamdata->{'swfVerify'} = $1;
      }
    }
#     print "$streamdata->{'url'}, $streamdata->{'playpath'}, $streamdata->{'swfUrl'}, $streamdata->{'swfVerify'}\n";

    $command = [
      'rtmpdump', '-o', $target, '-r', $streamdata->{'url'}, '-y', $streamdata->{'playpath'}, '-W', $streamdata->{'swfUrl'}
    ];
  }
  elsif ($stream->{'url'} =~ m/\.m3u8/)
  {
    $command = [
      'ffmpeg', '-i', $stream->{'url'}, '-acodec', 'copy', '-vcodec', 'copy', '-absf', 'aac_adtstoasc', $target
    ];
  }
  else
  {
    $command = [
      'wget', '-O', $target, $stream->{'url'}
    ];
  }

  make_path($dir);
  
  runCommand($command);
  
  if ($stream->{'meta'}->{'subtitles'})
  {
    my $sub = $dir . $item->{'title'} . '.srt';


#     if ($opt_v)
#     {
      print "Downloading sub: '$sub'\n";
#     }
    
    $command = [
      'wget', '-O', $sub, $stream->{'meta'}->{'subtitles'}
    ];
    runCommand($command);
  }
}

sub parseQuality
{
  my $item = shift;

  my $q = $item->{'meta'}->{'quality'};

  if ($q)
  {
    my @arr = split(/ /, $q);
    $q = $arr[0];
  }
  else
  {
    $q = 0
  }
  
  return $q;
}

sub handleItem
{
  my $dir = shift;
  my $item = shift;
  my $cfg = shift;

  my $url = "http://pirateplay.se/api/get_streams.js?url=" . uri_escape($item->{'link'});

  my $content = get($url);

  my $json = decode_json( $content );

  my @sorted = sort { parseQuality($b) <=> parseQuality($a) } @{$json};

  if ($cfg->{'quality'} == 0)
  {
    downloadItem($dir, $item, $sorted[0]);
  }
  elsif ($cfg->{'quality'} < 0)
  {
    downloadItem($dir, $item, $sorted[-1]);
  }
  else
  {
    foreach my $stream (@sorted)
    {
      if (parseQuality($stream) < $cfg->{'quality'})
      {
        downloadItem($dir, $item, $stream);
        last;
      }
    }
  }
}

##XML::RSS::LibXML
use XML::RSS::LibXML;

sub read_with_libxml
{
  my $globalpath = shift;
  my $rss = shift;
  my $data = shift;

  my $content = get($rss);

  my $parser = XML::RSS::LibXML->new;
  $parser->parse($content);

  my $time = $data->{'timestamp'};

  foreach my $item (@{ $parser->{items} })
  {
    my $date = UnixDate( ParseDate( $item->{'pubDate'} ), "%s");

    if ($date > $data->{'timestamp'})
    {
      while (my ($regex, $cfg) = each (%{$data->{'filters'}}))
      {
        if ($item->{'title'} =~ m/$regex/)
        {
          my $fullpath = $globalpath . '/' . $cfg->{'path'} . '/';
          handleItem($fullpath, $item, $cfg);
          last;
        }
      }
      if ($date > $time)
      {
        $time = $date;
      }
    }
  }
  return $time;
}

sub getUserInput
{
  my $url = shift;
  my $filter = shift;
  my $path = shift;
  my $quality = shift;
  my $globalpath = shift;

#   while (not $url)
  {
    print "Enter feed url: ";
    chomp ($url = <>);
  }

#   while (not $filter)
  {
    print "Enter (regex) filter for feed item title: ";
    chomp ($filter = <>);
  }

#   while (not $path)
  {
    print "Enter download path (relative from '$globalpath'): ";
    chomp ($path = <>);
  }

#   while (not $quality)
  {
    print "Enter desired quality: ";
    chomp ($quality = <>);
  }

  return ($url, $filter, $path, $quality);
}

sub getUserInputGlobal
{
  my $path = shift;
  my $newpath;

  print "Enter download path [$path]: ";
  chomp ($newpath = <>);

  $path = $newpath unless ($newpath);

  return $path;
}

sub saveConfig
{
  my $config = shift;
  my $file = shift;

#   my $json = encode_json(encode_utf8(qq($config)));
  my $json = JSON->new->utf8(0)->pretty(1)->encode($config);

  my $dir = dirname($file);
  make_path($dir);
  open my $fh, '>:encoding(UTF-8)', $file or die "Ouch: $!\n"; # now go do stuff w/file

  binmode $fh, ':encoding(UTF-8)';

  print $fh $json;
}

getopts('adlvnru:f:p:q:');

# a = add filter/feed
# d = delete filter/feed
# l = list filters/feeds
# v = verbose
# n = dry-run
# r = reset timestamp
# u = feed url, used with -a/-d/-l and normal run
# f = regex filter, used with -a/-d
# p = download path, used with -a/-d
# q = quality setting, used with -a/-d

my $config = File::HomeDir->my_data . '/.config/tvfeed/tvfeed.json';

my $json = read_file($config, err_mode => 'quiet');

if (not $json)
{
  $global = {
    'path'  => getUserInputGlobal(File::HomeDir->my_videos),
    'feeds' => {}
    };
}
else
{
  $global = decode_json( $json );
}

if ($opt_a or !%{$global->{'feeds'}})
{
  my ($url, $filter, $path, $quality) = getUserInput($opt_u, $opt_f, $opt_p, $opt_q, $global->{'path'});

  $global->{'feeds'}->{$url} = {
    'filters'   => { },
    'timestamp' => 0
  } unless $global->{'feeds'}->{$url};
  
  $global->{'feeds'}->{$url}->{'filters'}->{$filter} = {
    'path'    => $path,
    'quality' => $quality
  };

  if (!$opt_n)
  {
    saveConfig($global, $config);
  }

  if ($opt_a)
  {
    exit 0;
  }
}

if ($opt_d)
{
  if ($opt_u)
  {
    if ($opt_f)
    {
      delete $global->{'feeds'}->{$opt_u}->{'filters'}->{$opt_f};
      if (!%{$global->{'feeds'}->{$opt_u}->{'filters'}})
      {
        delete $global->{'feeds'}->{$opt_u};
      }
    }
    else
    {
      delete $global->{'feeds'}->{$opt_u};
    }
  }
  exit 0;
}

if ($opt_l)
{
  print "\nGlobal download path: $global->{'path'}\n";
  while (my ($rss, $setting) = each (%{$global->{'feeds'}}))
  {
    if (not $opt_u or $opt_u == $rss)
    {
      print "\nFeed url '$rss':\n";
      while (my ($filter, $setting2) = each (%{$setting->{'filters'}}))
      {
        print " Filter '$filter' downloads to '$setting2->{'path'}' with quality $setting2->{'quality'}\n";
      }
    }
  }
  exit 0;
}

# exit 0;
while (my ($rss, $setting) = each (%{$global->{'feeds'}}))
{
  if ($opt_r)
  {
    $setting->{'timestamp'} = 0;
  }
  if (not $opt_u or $opt_u == $rss)
  {
    my $t = read_with_libxml($global->{'path'}, $rss, $setting);
  }
  $setting->{'timestamp'} = $t;
}

# if (!$opt_n)
# {
  saveConfig($global, $config);
# }
